package Arrays_programs;

import java.util.Scanner;

public class Two_Dimensional_array {
	static Scanner sc = new Scanner(System.in);

	 public static int[][] Create_Array(int row_size, int col_size) {

	//  System.out.println("Enter Row Value");
	//  int row_size=sc.nextInt();//3
	//  System.out.println("Enter Column Value");
	//  int col_size=sc.nextInt();//3
	//  
	  int arr[][] = new int[row_size][col_size];

	  System.out.println("Enter " + row_size * col_size + " numbers ");
	  // getting input from user
	  for (int row = 0; row < row_size; row++) {
	   for (int col = 0; col < col_size; col++) {
	    arr[row][col] = sc.nextInt();
	   }

	  }

	  return arr;

	 }

	 public static void main(String[] args) {
		 Two_Dimensional_array ad= new  Two_Dimensional_array();

	  System.out.println("nums1 row_size");
	  int num1_row_size = sc.nextInt();
	  System.out.println("nums1 col_size");
	  int num1_col_size = sc.nextInt();
	  System.out.println("nums2 row_size");
	  int num2_row_size = sc.nextInt();
	  System.out.println("nums2 col_size");
	  int num2_col_size = sc.nextInt();

	  if (num1_col_size == num2_row_size && num1_row_size == num2_col_size)
	  {

	   System.out.println("Array 1");
	   int num1[][] = Create_Array(num1_row_size, num1_col_size);
	   System.out.println("Array 2");
	   int num2[][] = Create_Array(num2_row_size, num2_col_size);

	   for (int row = 0; row < num1.length; row++) {
	    for (int col = 0; col < num2.length; col++) {
	     System.out.print(num1[row][col] + num2[col][row] + " ");
	    }

	    System.out.println();

	   }

	  } else
	   System.out.println("Can not able to 2 array");

	 }

	}