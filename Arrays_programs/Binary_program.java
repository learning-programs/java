package Arrays_programs;

import java.util.Scanner;

public class Binary_program {
	public static void main(String[] args) {
		
	Binary_program bp= new Binary_program();
	   Scanner op = new Scanner(System.in);
	   System.out.println("Enter array size:");
	   int size=op.nextInt();
	   int arr[]= new int[size];
	   System.out.println("Enter "+size+ " numbers in Sorting Order : " );
	   for(int i=0;i<size;i++) {
	    arr[i]=op.nextInt();
	   }
	   System.out.println("Enter the target value : ");
	   int target=op.nextInt();
	   
	   int start=0;
	   int end=arr.length-1;
	   boolean condition = false;
	   
	   while(start<=end) {
	    int mid=(start+end)/2;
	    
	    if(arr[mid]==target) {
	     System.out.println("Your Target "+arr[mid]+" is found.");
	     condition=true;
	    }
	    if(arr[mid]<target) 
	     start=mid+1;
	    else
	     end=mid-1;
	     
	   }
	   if(condition==false)
	    System.out.println("your Target "+ target+" is not found");
	   
	 }
}
